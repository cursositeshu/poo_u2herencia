﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace U2Herencia.COMMON
{
    public class Deudor
    {
        public event EventHandler LimiteExedido;
        public event EventHandler DeudaSaldada;

        public Deudor(Cliente deudor)
        {
            adeudo = 0;
            abonos = new List<Abono>();
            cliente = deudor;
            ventasRelacionadas = new List<Venta>();
        }

        float adeudo;
        public float Adeudo
        {
            get
            {
                return adeudo;
            }

            private set
            {
            }
        }

        List<Abono> abonos;
        public List<Abono> Abonos
        {
            get
            {
                return abonos;
            }

            private set
            {
            }
        }

        Cliente cliente;
        public Cliente Cliente
        {
            get
            {
                return cliente;
            }

            private set
            {
            }
        }

        List<Venta> ventasRelacionadas;
        public List<Venta> VentasRelacionadas
        {
            get
            {
                return ventasRelacionadas;
            }

            private set
            {
            }
        }


        public float Abonar(Abono abono)
        {
            abonos.Add(abono);
            return CalcularSaldo();
        }

        private float CalcularSaldo()
        {
            float loQueDebe = 0;
            float loQuePago = 0;
            foreach (var item in ventasRelacionadas)
            {
                loQueDebe += item.Total;
            }
            foreach (var item in abonos)
            {
                loQuePago += item.Cantidad;
            }
            float saldo = loQueDebe - loQuePago;
            adeudo = saldo;
            if (saldo == 0)
            {
                DeudaSaldada(this,null);
            }
            if (saldo >= 1000)
            {
                LimiteExedido(this, null);
            }
            return saldo;
        }

        public float AgregarDeuda(Venta datosDeVenta)
        {
            ventasRelacionadas.Add(datosDeVenta);
            return CalcularSaldo();
        }
    }
}